/*********************************
* Class: MAGSHIMIM C2			 *
* Week:                			 *
* Name:                          *
* Credits:                       *
**********************************/

#include <iostream>
#include<stdio.h>
#include "prog.h"

void initQueue(queue* q, unsigned int size)
{
	q->maxSize = size;
	q->elements = new int[q->maxSize];
	q->count = 0;
}
void cleanQueue(queue* q)
{
	while(!isEmpty(q))
	{
		dequeue(q);
	}
	delete[] q->elements;
	printf("\nqueue is cleaned\n");
}
int isEmpty(queue* q)
{
	return (q->count== 0);
}
void enqueue(queue* q, unsigned int newValue)
{
	if (q->count == q->maxSize)// if the number of taken elements is equal to the numbers of general elements the queue is full
	{
		printf("the queue is full");
	}
	else 
	{
		q->elements[q->count] = newValue;
		q->count += 1;
	}
}
void print_queue(queue* q)
{
	int i = 0;
	printf("\n");
	for (i = 0; i < q->count; i++)
	{
		printf("%d",q->elements[i]);
		printf(",");
	}
	printf("\n");
}
int dequeue(queue* q)
{
	int data = q->elements[0], i = 0;
	for (i = 0; i < q->count-1; i++)
	{
		q->elements[i] = q->elements[i + 1];//rotating the queue to the left and "deleting" the last element
	}
	q->elements[i] = 0;
	return data;
}